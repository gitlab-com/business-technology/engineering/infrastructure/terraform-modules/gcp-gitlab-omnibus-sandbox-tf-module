# variables.tf

variable "owner_full_name" {
  type        = string
  description = "The first and last name of the owner (without special punctuation). (Example: Dade Murphy)"
}

variable "owner_email_handle" {
  type        = string
  description = "The first initial and last name email handle of the owner. (Example: dmurphy)"
}

variable "gl_dept" {
  type        = string
  description = "The GitLab department slug that the owner belongs to. See https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#gitlab-department-gl_dept for a list of expected values."
  default     = "undefined"
}

variable "gl_dept_group" {
  type        = string
  description = "The GitLab department group (team) slug that the owner belongs to. See https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#gitlab-department-group-gl_dept_group for a list of expected values."
  default     = "undefined"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
  default     = "us-central1"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: us-east1-c)"
  default     = "us-central1-c"
}

variable "gitlab_ci_cluster_ingress" {
  type = object({
    enabled     = bool
    external_ip = string
  })
  description = "The external IP address that is associated with the ingress service after the helm chart has been deployed."
  default = {
    "enabled" : "false",
    "external_ip" : "1.2.3.4"
  }
}

variable "gitlab_ci_cluster_letsencrypt_txt" {
  type = object({
    enabled = bool
    value   = string
  })
  description = "The certbot verification process will create a TXT string that needs to be added to the DNS zone to complete LetsEncrypt SSL verification."
  default = {
    "enabled" : "false",
    "value" : "placeholder"
  }
}
