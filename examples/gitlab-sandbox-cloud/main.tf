# [terraform-project]/main.tf

terraform {
  # backend {
  # ...
  # }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Define the Google Cloud Provider
provider "google" {
  project = var.gcp_project
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  project = var.gcp_project
}

# Sandbox Environment with VPC, DNS, GitLab Omnibus, and CI Cluster
module "gitlab_omnibus_sandbox" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module.git"

  env_name          = "${var.owner_full_name} Omnibus Sandbox Environment"
  env_prefix        = "${var.owner_email_handle}-"
  env_sandbox_mode  = true
  gcp_dns_zone_fqdn = "omnibus.gcp.${var.owner_email_handle}.gitlabsandbox.net."
  gcp_project       = var.gcp_project
  gcp_region        = var.gcp_region
  gcp_region_cidr   = var.gcp_region_cidr
  gcp_region_zone   = var.gcp_region_zone
  labels            = var.labels

}
