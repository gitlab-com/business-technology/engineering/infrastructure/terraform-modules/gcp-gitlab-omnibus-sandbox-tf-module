# [terraform-project]/variables.tf

variable "env_name" {
  type        = string
  description = "A friendly name to describe the environment. (Example: Dade Murphy Omnibus Sandbox Environment)"
}

variable "env_prefix" {
  type        = string
  description = "A alphadash short abbreviation of the environment name with a trailing dash. (Example: dmurphy-)"
}

variable "gcp_dns_zone_fqdn" {
  type        = string
  description = "The GCP Cloud DNS zone FQDN for this environment with a trailing period. (Example: omnibus.mydomain.tld.)"
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID that the instance is deployed in. (Example: my-project-name-a1b2c3d4)"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. us-east1)"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: us-east1-c)"
}

variable "gitlab_ci_cluster_ingress" {
  type = object({
    enabled     = bool
    external_ip = string
  })
  description = "The external IP address that is associated with the ingress service after the helm chart has been deployed."
  default = {
    "enabled" : "false",
    "external_ip" : "1.2.3.4"
  }
}

variable "gitlab_ci_cluster_letsencrypt_txt" {
  type = object({
    enabled = bool
    value   = string
  })
  description = "The certbot verification process will create a TXT string that needs to be added to the DNS zone to complete LetsEncrypt SSL verification."
  default = {
    "enabled" : "false",
    "value" : "placeholder"
  }
}
