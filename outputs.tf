# outputs.tf

output "env" {
  value = {
    env_name         = var.env_name
    env_prefix       = var.env_prefix
    env_sandbox_mode = var.env_sandbox_mode
  }
}

output "gcp" {
  value = {
    dns_zone_fqdn = var.gcp_dns_zone_fqdn
    project       = var.gcp_project
    region        = var.gcp_region
    region_cidr   = var.gcp_region_cidr
    region_zone   = var.gcp_region_zone
  }
}

output "gitlab_omnibus_instance" {
  value = {
    instance             = module.gitlab_omnibus_instance
    subdomain_dns_record = google_dns_record_set.gitlab_omnibus_subdomain_dns_record
    firewall_rule        = google_compute_firewall.gitlab_omnibus_firewall_rule
  }
}

output "gitlab_runner_manager_instance" {
  value = {
    instance      = module.gitlab_runner_manager_instance
    firewall_rule = google_compute_firewall.gitlab_runner_manager_firewall_rule
  }
}

output "gitlab_ci_cluster" {
  value = {
    cluster         = module.gitlab_ci_cluster
    ingress         = var.gitlab_ci_cluster_ingress
    letsencrypt_txt = var.gitlab_ci_cluster_letsencrypt_txt
  }
}

output "labels" {
  value = var.labels
}

output "network" {
  value = {
    vpc           = google_compute_network.vpc_network
    router        = google_compute_router.router
    nat_gateway   = google_compute_router_nat.nat_gateway
    region_subnet = google_compute_subnetwork.region_subnet
    dns_zone      = google_dns_managed_zone.dns_zone
  }
}
