# GitLab Omnibus Sandbox All-in-One Terraform Module

This Terraform module is designed to be used for creating an all-in-one GitLab sandbox environment with an Omnibus instance, CI cluster, storage buckets, and associated network configuration.

## Table of Contents

* [Version Compatibility](#version-compatibility)
* [Usage Instructions](#usage-instructions)
    * [Prerequisites](INSTALL.md#prerequisites)
    * [GitLab CI Variables](INSTALL.md#gitlab-ci-variables)
    * [Step-by-Step Instructions](INSTALL.md#step-by-step-instructions)
    * [Variables, Outputs, and Additional Customization](INSTALL.md#variables-outputs-and-additional-customization)
    * [Experiment Example](#experiment-example)
    * [Production Example](#production-example)
    * [GitLab Sandbox Cloud Example](#gitlab-sandbox-cloud-example)
* [Variables](#variables)
* [Outputs](#outputs)
* [Authors and Maintainers](#authors-and-maintainers)

## Version Compatibility

* Minimum Terraform v0.13
* Tested with Terraform v0.13, v0.14, v0.15
* See the [hashicorp/terraform CHANGELOG](https://github.com/hashicorp/terraform/blob/master/CHANGELOG.md) for future breaking and deprecation changes

## Usage Instructions

See [INSTALL.md](INSTALL.md) for step-by-step instructions for using this Terraform module.

### Experiment Example

This example includes a basic configuration to get you started with using this module for the first time or a proof-of-concept use case.
* [examples/experiment/main.tf](examples/experiment/main.tf)
* [examples/experiment/outputs.tf](examples/experiment/outputs.tf)
* [examples/experiment/terraform.tfvars.json](examples/experiment/terraform.tfvars.json)
* [examples/experiment/variables.tf](examples/experiment/variables.tf)

### Production Example

This example includes full use of variables for you to easily copy and paste into your own environment configuration and start using this module for any use case.
* [examples/production/main.tf](examples/production/main.tf)
* [examples/production/outputs.tf](examples/production/outputs.tf)
* [examples/experiment/terraform.tfvars.json](examples/experiment/terraform.tfvars.json)
* [examples/production/variables.tf](examples/production/variables.tf)

### GitLab Sandbox Cloud Example

This example includes the best practice configuration for GitLab team members that are using the GitLab Sandbox Cloud with their own GCP project. This has been customized with the GitLab infrastructure standards labels configuration. If you are not using the GitLab Sandbox Cloud, you should use the `production` example for implementing this module in your environment.
* [examples/gitlab-sandbox-cloud/main.tf](examples/gitlab-sandbox-cloud/main.tf)
* [examples/gitlab-sandbox-cloud/outputs.tf](examples/gitlab-sandbox-cloud/outputs.tf)
* [examples/experiment/terraform.tfvars.json](examples/experiment/terraform.tfvars.json)
* [examples/gitlab-sandbox-cloud/variables.tf](examples/gitlab-sandbox-cloud/variables.tf)

## Variables

We use top-level variables where possible instead of maps to allow easier handling of default values with partially defined maps, and reduce complexity for developers who are just getting started with Terraform syntax.

<table>
<thead>
<tr>
    <th style="width: 25%;">Variable Key</th>
    <th style="width: 40%;">Description</th>
    <th style="width: 10%;">Required</th>
    <th style="width: 25%;">Example Value</th>
</tr>
</thead>
<tbody>
<tr>
    <td>
        <code>env_name</code>
    </td>
    <td>A friendly name to describe the environment.</td>
    <td><strong>Yes</strong></td>
    <td><code>Dade Murphy Omnibus Sandbox Environment</code></td>
</tr>
<tr>
    <td>
        <code>env_prefix</code>
    </td>
    <td>A alphadash short abbreviation of the environment name with a trailing dash.</td>
    <td><strong>Yes</strong></td>
    <td><code>dmurphy</code></td>
</tr>
<tr>
    <td>
        <code>env_sandbox_mode</code>
    </td>
    <td>If sandbox mode is enabled, infrastructure resources are downsized to accomodate less than 10 users and are cost efficient with machine types and high-availability requirements. See the gitlab_omnibus and gitlab_ci_cluster variable defaults below for sandbox mode values.</td>
    <td><strong>No</strong></td>
    <td><code>true</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>env_sandbox_mode</code>
    </td>
    <td>If sandbox mode is enabled, infrastructure resources are downsized to accomodate less than 10 users and are cost efficient with machine types and high-availability requirements. See the gitlab_omnibus and gitlab_ci_cluster variable defaults below for sandbox mode values.</td>
    <td><strong>No</strong></td>
    <td><code>true</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gcp_dns_zone_fqdn</code>
    </td>
    <td>The FQDN of the GCP Cloud DNS zone that will be created for this environment, with a trailing period.</td>
    <td><strong>Yes</strong></td>
    <td><code>gitlab.example.tld.</code></td>
</tr>
<tr>
    <td>
        <code>gcp_project</code>
    </td>
    <td>The <a target="_blank" href="https://cloud.google.com/resource-manager/docs/creating-managing-projects">GCP project ID</a> (may be a alphanumeric slug).</td>
    <td><strong>Yes</strong></td>
    <td>
        <code>123456789012</code><br /><br />
        <code>my-project-name</code>
    </td>
</tr>
<tr>
    <td>
        <code>gcp_region</code>
    </td>
    <td>The <a target="_blank" href="https://cloud.google.com/compute/docs/regions-zones">GCP region</a> that the resources will be deployed in.</td>
    <td><strong>Yes</strong></td>
    <td><code>us-east1</code></td>
</tr>
<tr>
    <td>
        <code>gcp_region_cidr</code>
    </td>
    <td>A /12 CIDR range for the GCP region that will be used for dynamically creating subnets. You can leave this at the default value for most use cases. (Ex. 10.128.0.0/12, 10.144.0.0/12, 10.160.0.0/12)</td>
    <td>No</td>
    <td><code>10.128.0.0/12</code> <small>(default)</small></td>
</tr>
<tr>
    <td>
        <code>gcp_region_zone</code>
    </td>
    <td>The <a target="_blank" href="https://cloud.google.com/compute/docs/regions-zones">GCP region availability zone</a> that the resources will be deployed in. This must match the region.</td>
    <td><strong>Yes</strong></td>
    <td><code>us-east1-c</code></td>
</tr>
<tr>
<tr>
    <td>
        <code>gitlab_omnibus</code>
    </td>
    <td>The sizing values for custom-sized GitLab Omnibus instance. These values are ignored if `env_sandbox_mode` is `true`.</td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>"disk_boot_size" : "30"</code><br />
        <code>"disk_storage_size" : "100"</code><br />
        <code>"gcp_deletion_protection" : "false"</code><br />
        <code>"gcp_image" : "ubuntu-1804-lts"</code><br />
        <code>"gcp_machine_type" : "e2-standard-4"</code><br />
        <code>}</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>gitlab_ci_cluster</code>
    </td>
    <td>The sizing values for custom-sized GitLab CI Kubernetes instance. These values are ignored if `env_sandbox_mode` is `true`.</td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>"gcp_machine_type" : "e2-standard-2"</code><br />
        <code>"gcp_preemptible_nodes" : "true"</code><br />
        <code>"gke_autoscaling_profile" : "BALANCED"</code><br />
        <code>"gke_cluster_type" : "zonal"</code><br />
        <code>"gke_maintenance_start_time" : "03:00"</code><br />
        <code>"gke_release_channel" : "REGULAR"</code><br />
        <code>"gke_version_prefix" : "1.18."</code><br />
        <code>}</code><br />
    </td>
</tr>
<tr>
    <td>
        <code>labels</code>
    </td>
    <td>Labels to place on the GCP infrastructure resources. Label keys should use underscores, values should use alphadash format. No spaces are allowed. GitLab team members should use <a target="_blank" href="https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/">infrastructure standards labels</a></td>
    <td>No</td>
    <td>
        <code>{</code><br />
        <code>"gl_env_type"           = "experiment"</code><br />
        <code>"gl_env_name"           = "cool-product-app-server"</code><br />
        <code>"gl_owner_email_handle" = "jmartin"</code><br />
        <code>"gl_realm"              = "sandbox"</code><br />
        <code>"gl_dept"               = "sales-cs"</code><br />
        <code>"gl_dept_group"         = "sales-cs-sa-us-west"</code><br />
        <code>}</code><br />
    </td>
</tr>
</tbody>
</table>

## Outputs

The outputs are returned as a map with sub arrays that use dot notation. You can see how each output is defined in [outputs.tf](outputs.tf).

```hcl
# Get a map with all values for the module
module.gitlab_omnibus_sandbox

# Get individual values
TODO
```

## Authors and Maintainers

* Jeff Martin / @jeffersonmartin / jmartin@gitlab.com
